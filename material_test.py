import bpy


class ExportSomeData(bpy.types.Operator):
    """Test exporter which just writes the object currently selected, followed by all objects in the scene"""
    bl_idname = "export.some_data"
    bl_label = "Export Some Data"

    filepath = bpy.props.StringProperty(subtype="FILE_PATH")

    @classmethod
    def poll(cls, context):
        return context.object is not None

    def execute(self, context):
        file = open(self.filepath + ".txt", 'w')
        file.write("Hello World, I am a file exporter. This is the object you are clicking: " + context.object.name + '\n')
        file.write("Your current scene contains the following objects: ")
        for obj in bpy.data.objects:
            file.write('\n' + obj.name)
            if obj.type == 'MESH':
                for mat in obj.data.materials:
                    file.write('\n'+ mat.name + '\n')
                    for i in mat.diffuse_color:
                        file.write(str(i) + " ")
        return {'FINISHED'}

    def invoke(self, context, event):
        context.window_manager.fileselect_add(self)
        return {'RUNNING_MODAL'}


# Only needed if you want to add into a dynamic menu
def menu_func(self, context):
    self.layout.operator_context = 'INVOKE_DEFAULT'
    self.layout.operator(ExportSomeData.bl_idname, text="Text Export Operator")

# Register and add to the file selector
bpy.utils.register_class(ExportSomeData)
#bpy.types.INFO_MT_file_export.append(menu_func)


# test call
bpy.ops.export.some_data('INVOKE_DEFAULT')