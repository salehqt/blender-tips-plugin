#Code to cut out specific sections of large organs within a bounding box

import bpy
from bpy.props import StringProperty, BoolProperty, EnumProperty

C = bpy.context
D = bpy.data
O = bpy.ops

def main(context):
    #Adds a modifier to the active object
    O.object.modifier_add(type='BOOLEAN')
    #Create a cube and rename it as "Boundary"    
    #This object is set to intersect the selected object
    C.object.modifiers[0].object = bpy.data.objects["Boundary"]

    #Applying the modifier creates a new object that is the intersection
    O.object.modifier_apply(apply_as='DATA', modifier="Boolean")
    #Removes the boundary object
    C.scene.objects.unlink(bpy.data.objects["Boundary"])


class ViewBoundSelection(bpy.types.Operator):
    """Tooltip"""
    bl_idname = "object.simple_operator"
    bl_label = "View Bound Selection"

    @classmethod
    def poll(cls, context):
        return context.active_object is not None

    def execute(self, context):
        main(context)
        return {'FINISHED'}


def register():
    bpy.utils.register_class(SimpleOperator)


def unregister():
    bpy.utils.unregister_class(SimpleOperator)


if __name__ == "__main__":
    register()

    # test call
    bpy.ops.object.simple_operator()
