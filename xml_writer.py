import bpy
import xml.etree.ElementTree as ET

#main method for creating file
def createFile():
 root = ET.Element("scene")
 root.set("xmlns", "http://surflab.cise.ufl.edu/TIPS/scene")
  
#go through objects
 for obj in bpy.data.objects:
  if obj.type == 'CAMERA':
   camera(root, obj)
  elif obj.type == 'SCENE':
   continue
  elif obj.type == 'LAMP':
   continue
  else:
   object(root, obj)
 
 return root
 
#deal with camera
def camera(xmlNode, cam):
 camera = ET.SubElement(xmlNode, "camera")
 transform = ET.SubElement(camera, "transform")
 arr = cam.data.view_frame()
 #print(arr)
 
 ret = ""
 for i in arr:
  for j in i:
   ret += str(j) + " "
  ret += '\n'
 transform.text = ret
  
#deal with generic object 
def object(xmlNode, obj): 
 object = ET.SubElement(xmlNode, "object")
 object.set("name", obj.name)
 
 ret = ""
 rotation = ET.SubElement(object, "rotation")
 for i in obj.delta_rotation_quaternion:
  ret += str(i) + " " 
 rotation.text = ret
 
 ret = ""
 scale = ET.SubElement(object, "scale")
 for i in obj.delta_scale:
  ret += str(i) + " "
 scale.text = ret
 
 #for i in obj.data.meshes:
  #geo = ET.SubElement(object, "geometry")
  #geometry(geo, i)
 for i in obj.data.materials:
  mat = ET.SubElement(object, "material")
  material(mat, i)

#deal with geometry  
def geometry(parent, geo):
 d = 0
 
#deal with material
def material(parent, mat):
 ret = ""
 diffuse = ET.SubElement(parent, "color")
 diffuse.set("type", "diffuse")
 for i in mat.diffuse_color:
  ret += str(i) + " " 
 diffuse.text = ret
 
 ret = ""
 specular = ET.SubElement(parent, "color")
 specular.set("type", "specular")
 for i in mat.specular_color:
  ret += str(i) + " " 
 specular.text = ret
 
 if mat.active_texture.type == 'IMAGE':
  texture = ET.SubElement(parent, "texture")
  texture.set("filename", mat.active_texture.filepath)

#class for writing
class XMLExporter(bpy.types.Operator):
 bl_idname = "export.xmldata"
 bl_label = "Export data as XML"
 filepath = bpy.props.StringProperty(subtype="FILE_PATH")

 @classmethod
 def poll(cls, context):
  return context.object is not None

 def execute(self, context):
  file = open(self.filepath + ".xml", 'w')
  file.write(str(createFile()))
  file.close()
  return {'FINISHED'}

 def invoke(self, context, event):
  context.window_manager.fileselect_add(self)
  return {'RUNNING_MODAL'}


# Only needed if you want to add into a dynamic menu
def menu_func(self, context):
  self.layout.operator_context = 'INVOKE_DEFAULT'
  self.layout.operator(XMLExporter.bl_idname, text="XML Exporter")

# Register and add to the file selector
bpy.utils.register_class(XMLExporter)
#bpy.types.INFO_MT_file_export.append(menu_func)


# test call
bpy.ops.export.xmldata('INVOKE_DEFAULT')
