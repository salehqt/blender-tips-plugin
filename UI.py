import bpy
import xml.etree.ElementTree as ET
import array as A
import bpy_extras

D = bpy.data
O = bpy.ops
C = bpy.context
IO = bpy_extras.io_utils

def openScreen():
  scr = O.screen.new()
  scr.use_play_3d_editors = True
  scr.use_animation_editors = True
  scr.use_play_image_editors = True
  

class TIPS_UI_Operator(bpy.types.Operator):
  '''New UI Operator'''
  bl_idname = "screen.tipsformat"
  bl_label = "TIPS UI Format"

  @classmethod
  def poll(cls, context):
    return context.object is not None

  def execute(self, context):
    openScreen()
    file.close()
    return {'FINISHED'}

  def invoke(self, context, event):
    context.window_manager.fileselect_add(self)
    return {'RUNNING_MODAL'}

# Only needed if you want to add into a dynamic menu
def menu_func(self, context):
    self.layout.operator_context = 'INVOKE_DEFAULT'
    self.layout.operator(TIPS_UI_Operator.bl_idname, text="TIPS UI format")

    
def register():
    bpy.utils.register_class(TIPS_UI_Operator)
    #bpy.types.INFO_MT_file_import.append(menu_func)
   
def unregister():
    bpy.utils.unregister_class(TIPS_UI_Operator)
    #bpy.types.INFO_MT_file_import.remove(menu_func)

if __name__ == "__main__":
    register()

O.screen.tipsformat('INVOKE_DEFAULT')