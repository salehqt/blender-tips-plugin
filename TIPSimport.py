import bpy
import xml.etree.ElementTree as ET
import array as A
import bpy_extras
import re
import xml.sax as sax
import time

D = bpy.data
O = bpy.ops
C = bpy.context
IO = bpy_extras.io_utils

class XMLHandler(sax.handler.ContentHandler):
  def startElement(self, name, attrs):
    print(name)
    for k, v in attrs.items():
      print("\t",k, " : " , v)
  def endElement(self, name):
    print(name)
  def characters(self, content):
    i = re.finditer(r'([+-]?\d+(?:\.\d+)?)', content)
    try:
      while(True):
        n = next(i)
        print("Number: ", float(n.group(1)))
    except StopIteration:
      pass
    pass

#parse the text for data  
def parseText(text, mode, tuple=1):
  i = re.finditer(r'([+-]?\d+(?:\.\d+)?)', text)
  arr = A.array(mode)
  try:
    while(True):
      n = next(i)
      if mode == 'f':
        arr.append(float(n.group(tuple)))
      elif mode == 'I' or mode == 'i':
        arr.append(int(n.group(tuple)))
      else:
        print("Invalid character or not currently supported")
  except StopIteration:
    pass
  return arr

def parseTextMulti(text, tuple):
  str = text.split()
  size = int(len(str)/tuple)
  ret = [[0 for x in range(tuple)] for x in range(size)]
  k=0
  for i in range(size):
    for j in range(tuple):
      ret[i][j] = str[k]
      k = k+1
  return ret

#main method
def importFromXML(input):
  
  #parse the tree
  tree = ET.parse(input)
  root = tree.getroot()
  
  if root.attrib['content_type'] == 'object':
    obj = addMesh(root.find('object'))
    link(obj, bpy.context.scene)
  else:
    #go through the tree and add to the scene
    for scene in root:
      bpy.ops.scene.new()
      for child in scene:
        if child.tag == "camera":
          obj = addCamera(child)
        elif child.tag == 'lamp':
          obj = addLamp(child)
        else:
          obj = addMesh(child)
        link(obj, bpy.context.scene)
  
#create object
def create(nameInput, collection, type=None):
  #create datablock
  if type == None:
    data = collection.new(name=nameInput)
  else:
    data = collection.new(name=nameInput, type=type)
  
  #create object to hold datablock
  obj = bpy.data.objects.new(name=nameInput, object_data=data)
  
  return obj

#link to scene
def link(obj, scene):
  scene.objects.link(obj)

def LRS(xmlNode, obj, mode1='f', mode2='f', mode3='f'):  
  if xmlNode.tag == 'location':
    obj.location = parseText(xmlNode.text, mode1)
    return 1    
  elif xmlNode.tag == 'rotation':
    obj.rotation_mode = 'QUATERNION'
    obj.rotation_quaternion = parseText(xmlNode.text, mode2)
    return 1
  elif xmlNode.tag == 'scale':
    obj.scale = parseText(xmlNode.text, mode3)
    return 1
  return 0

#add camera to scene
def addCamera(xmlNode):
  obj=create(xmlNode.attrib['name'], D.cameras)
  
  for child in xmlNode:
    LRS(child, obj)
  
  return obj

#add lamp to scene
def addLamp(xmlNode):
  obj=create(xmlNode.attrib['name'], D.lamps, xmlNode.attrib['type'])
  
  for child in xmlNode:
    LRS(child, obj)
    
  return obj

#add mesh to scene
def addMesh(xmlNode):
  obj=create(xmlNode.attrib['name'], bpy.data.meshes)
  
  for child in xmlNode:
    if LRS(child, obj) == 1:
      continue
    elif child.tag == 'material':
      doMaterial(child, obj)
    elif child.tag == 'geometry':
      doGeometry(child, obj)
    elif child.tag == 'custom_property':
      doCustom(child, obj)
  
  return obj

#parse and define the material for object
def doMaterial(xmlNode, obj):
  aggr = obj.data.materials
  mat = bpy.data.materials.new("new_material")
  for child in xmlNode:
    if child.tag == 'color':
      if child.attrib['type'] == 'diffuse':
        mat.diffuse_color = parseText(child.text, 'f')
      elif child.attrib['type'] == 'specular':
        mat.specular_color = parseText(child.text, 'f')
    elif child.tag == 'texture':
      #continue
      if child.attrib != {}:
        image = bpy.data.images.load(child.attrib['filename'])
        tex = bpy.data.textures.new(name="new_texture", type = 'IMAGE')
        tex.image = image
        slot = mat.texture_slots.add()
        slot.texture = tex
    elif child.tag == 'custom_property':
      doCustom(child, mat)
  aggr.append(mat)

#parse and define the geometry for object
def doGeometry(xmlNode, obj):
  vertices = obj.data.vertices
  edges = obj.data.edges
  loops = obj.data.loops
  tessfaces = obj.data.tessfaces
  arr = [[]]
  size = 0
  
  for child in xmlNode:
    if child.tag == 'vertices':
      arr = parseTextMulti(child.text, int(child.attrib['stride']))
      vertices.add(len(arr))
      tuple = int(child.attrib['stride'])
      for i in range(len(arr)):
        for j in range(tuple):
          vertices[i].co[j] = float(arr[i][j])
      '''i = re.finditer(r'([+-]?\d+(?:\.\d+)?)', child.text)
      k=0
      tuple = int(child.attrib['stride'])
      i = re.finditer(r'([+-]?\d+(?:\.\d+)?)', child.text)
      try:
        while(True):
          n = next(i)
          k = k+1
      except StopIteration:
        pass      
      i = re.finditer(r'([+-]?\d+(?:\.\d+)?)', child.text)
      vertices.add(int(k/tuple))
      try:
        j=0
        while(True):
          for k in range(tuple):
            n = next(i)
            vertices[j].co[k] = float(n.group(1))
          j = j+1
      except StopIteration:
        pass'''
    elif child.tag == 'primitive-collection':
      arr = parseTextMulti(child.text, 4)
      tessfaces.add(len(arr))
      tuple = 4
      for i in range(len(arr)):
        for j in range(tuple):
          tessfaces[i].vertices_raw[j] = int(arr[i][j])
      obj.data.update()
      '''k=0
      tuple = 4 
      i = re.finditer(r'([+-]?\d+(?:\.\d+)?)', child.text)
      try:
        while(True):
          n = next(i)
          k = k+1
      except StopIteration:
        pass
      tessfaces.add(int(k/tuple))
      i = re.finditer(r'([+-]?\d+(?:\.\d+)?)', child.text)
      try:
        j=0
        while(True):
          for k in range(tuple):
            n = next(i)
            tessfaces[j].vertices_raw[k] = int(n.group(1))
          j = j+1
      except StopIteration:
        pass
      obj.data.update()'''
    else:
      print("Not supported")

def doCustom(xmlNode, obj):
  obj[xmlNode.attrib['name']] = float(xmlNode.attrib['value'])
      
#UI linker for Blender
class TIPSImportOperator(bpy.types.Operator):
  """An operator that imports all the XML data in TIPS format"""
  bl_idname = "import.tips_xml"
  bl_label = "Import from TIPS XML format"

  filepath = bpy.props.StringProperty(subtype="FILE_PATH")

  @classmethod
  def poll(cls, context):
    return context is not None

  def execute(self, context):
    file = open(self.filepath)
    importFromXML(file)
    file.close()
    return {'FINISHED'}

  def invoke(self, context, event):
    context.window_manager.fileselect_add(self)
    return {'RUNNING_MODAL'}

'''# Only needed if you want to add into a dynamic menu
def menu_func(self, context):
    self.layout.operator_context = 'INVOKE_DEFAULT'
    self.layout.operator(TIPSImportOperator.bl_idname, text="TIPS XML format")

    
def register():
    bpy.utils.register_class(TIPSImportOperator)
    #bpy.types.INFO_MT_file_import.append(menu_func)
   
def unregister():
    bpy.utils.unregister_class(TIPSImportOperator)
    #bpy.types.INFO_MT_file_import.remove(menu_func)

if __name__ == "__main__":
    register()

O.import_scene.tipsxml('INVOKE_DEFAULT')'''
