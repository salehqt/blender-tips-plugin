
bl_info = { 
    'name': "TIPS Simulation Import/Export plugin",
    'author': "SurfLab team",
    'version': (0, 0,  0),
    'blender': (2, 68, 0),
    'location': "",
    'warning': "",
    'description': "XML import/export from Blender scenes for the TIPS project",
    'wiki_url': "https://bitbucket.org/salehqt/tips-blender-plugin/wiki/",
    'tracker_url': "https://bitbucket.org/salehqt/tips-blender-plugin/",
    'category': 'Mesh'
}

from .TIPSexport import TIPSExportSceneOperator, TIPSExportModelOperator, TIPSExportAllScenesOperator, TIPSExportLibraryOperator
from .TIPSimport import TIPSImportOperator
from .TIPSpanel import TIPSPanel

import bpy


# Only needed if you want to add into a dynamic menu
def export_scene(self, context):
  self.layout.operator(TIPSExportSceneOperator.bl_idname, text="Scene: TIPS XML format")

def export_model(self, context):
  self.layout.operator(TIPSExportModelOperator.bl_idname, text="Model: TIPS XML format")

def export_all(self, context):
  self.layout.operator(TIPSExportAllScenesOperator.bl_idname, text="All Scenes: TIPS XML format")
  
def export_lib(self, context):
  self.layout.operator(TIPSExportLibraryOperator.bl_idname, text="Library: TIPS XML format")

def _import(self, context):
  self.layout.operator(TIPSImportOperator.bl_idname, text="File: TIPS XML format")
  
def register():
  bpy.utils.register_class(TIPSExportModelOperator)
  bpy.types.INFO_MT_file_export.append(export_model)
  
  bpy.utils.register_class(TIPSExportSceneOperator)
  bpy.types.INFO_MT_file_export.append(export_scene)
  
  bpy.utils.register_class(TIPSExportAllScenesOperator)
  bpy.types.INFO_MT_file_export.append(export_all)
  
  bpy.utils.register_class(TIPSExportLibraryOperator)
  bpy.types.INFO_MT_file_export.append(export_lib)
  
  bpy.utils.register_class(TIPSImportOperator)
  bpy.types.INFO_MT_file_import.append(_import)
  
  bpy.utils.register_class(TIPSPanel)
  
def unregister():
  bpy.utils.unregister_class(TIPSExportModelOperator)
  bpy.types.INFO_MT_file_export.remove(export_model)
  
  bpy.utils.unregister_class(TIPSExportSceneOperator)
  bpy.types.INFO_MT_file_export.remove(export_scene)
  
  bpy.utils.unregister_class(TIPSExportAllScenesOperator)
  bpy.types.INFO_MT_file_export.remove(export_all)
  
  bpy.utils.unregister_class(TIPSExportLibraryOperator)
  bpy.types.INFO_MT_file_export.remove(export_lib)
  
  bpy.utils.unregister_class(TIPSImportOperator)
  bpy.types.INFO_MT_file_import.remove(_import)
  
  bpy.utils.unregister_class(TIPSPanel)

if __name__ == "__main__":
  register()