import bpy
import xml.etree.ElementTree as ET
import array

def exportToXML(out):
  root = ET.Element("scene")
  ## The XML namespace causes all the tag names to become FQN when
  ## importing, until we find out how to elimitae the namespaces when
  ## importing, we will not use xmlns.
  #root.set("xmlns", "http://surflab.cise.ufl.edu/TIPS/scene")
  
  #go through objects
    #accessing data from the currently loaded blend file w/ bpy.data
  for o in bpy.data.objects:
    temp = object(root, o)
    if o.type == 'CAMERA':
      camera(temp, o)
    elif o.type == 'MESH':
      mesh(temp, o)
    elif o.type == 'LAMP':
      lamp(temp, o)
    else:
      continue
 
  ET.ElementTree(root).write(out)
 
 
 
#deal with generic object
def object(xmlNode, obj): 
  objMapping = { 'MESH' : 'object', 'CAMERA' : 'camera' , 'LAMP': 'lamp' }
  object = ET.SubElement(xmlNode, objMapping[obj.type])
  object.set("name", obj.name)
  obj.rotation_mode = "QUATERNION"
 
  location = ET.SubElement(object, "location")
  iterate(location, obj.location)
 
  rotation = ET.SubElement(object, "rotation")
  iterate(rotation, obj.rotation_quaternion)
 
  scale = ET.SubElement(object, "scale")
  iterate(scale, obj.delta_scale)
 
  return object

#deal with camera
def camera(parent, obj):
  pass

#deal with mesh
def mesh(parent, obj):
  data = obj.data
  for i in data.materials:
    mat = ET.SubElement(parent, "material")
    material(mat, i)
  geo = ET.SubElement(parent, "geometry")
  geometry(geo, data)

#deal with lamp
def lamp(parent, obj):
  parent.set("type", obj.data.type)
  
#generic iterator
def iterate(xmlNode, comp):
  ret = ""
  for i in comp:
    ret += str(i) + " "
  xmlNode.text = ret

#coordinate iterator
def XYZiterate(xmlNode, comp, type=None):
  ret = ""
  if(type == None):
    for i in comp:
      ret += str(i.x) + " " + str(i.y) + " " + str(i.z)
      ret += "\n"
  else:
    for i in comp:
      ret += str(i.type.x) + " " + str(i.type.y) + " " + str(i.type.z)
      ret += "\n"
  xmlNode.text = ret

#deal with geometry
def geometry(parent, geo):
  vs = ET.SubElement(parent, "vertices")
  vs.set("stride", "3")
  #XYZiterate(vs, geo.vertices, co)
  writ = ""
  for i in geo.vertices:
    writ += str(i.co.x) + " " + str(i.co.y) + " " + str(i.co.z)
    writ += "\n"
  vs.text = writ
  
  nor = ET.SubElement(parent, "normals")
    #"stride"?
  nor.set("stride", "3")
  #XYZiterate(nor, geo.vertices, normal)
  writ = ""
  for i in geo.vertices:
    writ += str(i.normal.x) + " " + str(i.normal.y) + " " + str(i.normal.z)
    writ += "\n"
  nor.text = writ
  
  j = 0
  for i in geo.uv_layers:
    uvl = i.data
    writ = ""
    uvLoc = ET.SubElement(parent, "texcoords")
    uvLoc.set("stride", "2")
    uvLoc.set("tag", str(j))
    mapping = array.array('I')
    for i in geo.vertices:
      mapping.append(0)
    for  l in geo.loops:
      mapping[l.vertex_index] = l.index
    for i in mapping:
      writ += str(i) + " " + str(uvl[i].uv.x) + " " + str(uvl[i].uv.y)
      writ += "\n"
    uvLoc.text = writ
    j = j + 1
  
  indices = ET.SubElement(parent,"primitive-collection")
  indices.set("type","triangles")
  w = ""
  for i in geo.polygons:
    if i.loop_total == 3:
      w += str(i.vertices[0]) + " " + str(i.vertices[1]) + " " + str(i.vertices[2]) + "\n"
    elif i.loop_total == 4:
      w += str(i.vertices[0]) + " " + str(i.vertices[1]) + " " + str(i.vertices[2]) + "\n"
      w += str(i.vertices[0]) + " " + str(i.vertices[2]) + " " + str(i.vertices[3]) + "\n"
  indices.text = w

#deal with material
def material(parent, obj):
  diffuse = ET.SubElement(parent, "color")
  diffuse.set("type", "diffuse")
  iterate(diffuse, obj.diffuse_color)
  
  specular = ET.SubElement(parent, "color")
  specular.set("type", "specular")
  iterate(specular, obj.specular_color)

  tex = ET.SubElement(parent, "texture")
  texture(tex, obj)

#deal with texture
def texture(parent, obj):
  for i in obj.texture_slots:
    if(i != None): 
      tex = i.texture
      if(tex.type == 'IMAGE'):
        parent.set("filename", tex.image.filepath)

class TIPSExportOperator(bpy.types.Operator):
  """An operator that exprots all the XML data"""
  bl_idname = "export.tipsxml"
  bl_label = "Export to TIPS XML format"

  filepath = bpy.props.StringProperty(subtype="FILE_PATH")

  @classmethod
  def poll(cls, context):
    return context.object is not None

  def execute(self, context):
    file = open(self.filepath, 'wb')
    exportToXML(file)
    file.close()
    return {'FINISHED'}

  def invoke(self, context, event):
    context.window_manager.fileselect_add(self)
    return {'RUNNING_MODAL'}


