import bpy
import xml.etree.ElementTree as ET
import array as A
import bpy_extras

D = bpy.data
O = bpy.ops
C = bpy.context
IO = bpy_extras.io_utils

#main method
def importFromXML(input):
  
  #parse the tree
  tree = ET.parse(input)
  root = tree.getroot()
  
  #create scene
  scene = bpy.ops.scene.new()
  
  #go through the tree and add to the scene
  for child in root:
    if child.tag == "camera":
      addCamera(child)
    elif child.tag == 'lamp':
      addLamp(child)
    else:
      addMesh(child)

#parse the text for data  
def parseText(text, mode):
  str = text.split()
  arr = A.array(mode)
  if mode == 'f':
    for i in str:
      arr.append(float(i))
  elif mode == 'I' or mode == 'i':
    for i in str:
      arr.append(int(i))
  else:
    print("Invalid character or not currently supported")
  return arr

#create object and link it to desired scene
def createAndLink(nameInput, collection, type=None):
  #create datablock
  if type == None:
    data = collection.new(name=nameInput)
  else:
    data = collection.new(name=nameInput, type=type)
  
  #create object to hold datablock
  obj = D.objects.new(name=nameInput, object_data=data)
  
  #link to scene
  C.scene.objects.link(obj)
  
  #make it active
  obj.select = True
  C.scene.objects.active = obj
  
  #return the object to manipulate state
  return obj

def LRS(xmlNode, obj, mode1='f', mode2='f', mode3='f'):  
  if xmlNode.tag == 'location':
    obj.location = parseText(xmlNode.text, mode1)
    return 1    
  elif xmlNode.tag == 'rotation':
    obj.rotation_mode = 'QUATERNION'
    obj.rotation_quaternion = parseText(xmlNode.text, mode2)
    return 1
  elif xmlNode.tag == 'scale':
    obj.scale = parseText(xmlNode.text, mode3)
    return 1
  return 0

   #add camera to scene
def addCamera(xmlNode):
  obj=createAndLink(xmlNode.attrib['name'], D.cameras)
  
  for child in xmlNode:
    LRS(child, obj)

#add lamp to scene
def addLamp(xmlNode):
  obj=createAndLink(xmlNode.attrib['name'], D.lamps, xmlNode.attrib['type'])
  
  for child in xmlNode:
    LRS(child, obj)

#add mesh to scene
def addMesh(xmlNode):
  obj=createAndLink(xmlNode.attrib['name'], D.meshes)
  
  for child in xmlNode:
    if LRS(child, obj) == 1:
      continue
    elif child.tag == 'material':
      doMaterial(child, obj)
    elif child.tag == 'geometry':
      doGeometry(child, obj)

#parse and define the material for object
def doMaterial(xmlNode, obj):
  aggr = obj.data.materials
  mat = D.materials.new("new_material")
  for child in xmlNode:
    if child.tag == 'color':
      if child.attrib['type'] == 'diffuse':
        mat.diffuse_color = parseText(child.text, 'f')
      elif child.attrib['type'] == 'specular':
        mat.specular_color = parseText(child.text, 'f')
    elif child.tag == 'texture':
      if child.attrib != {}:
        image = D.images.load(child.attrib['filename'])
        tex = D.textures.new(name="new_texture", type = 'IMAGE')
        tex.image = image
        slot = mat.texture_slots.add()
        slot.texture = tex
  aggr.append(mat)

#parse and define the geometry for object
def doGeometry(xmlNode, obj):
  vertices = obj.data.vertices
  edges = obj.data.edges
  loops = obj.data.loops
  polys = obj.data.polygons
  arr = []
  size = 0
  
  #initialize vertices
  for child in xmlNode:
    if child.tag == 'vertices' or child.tag == 'normals':
      arr = parseText(child.text, 'f')
      size = len(arr)
      vertices.add(size/3)
      break
  
  for child in xmlNode:
    if child.tag == 'vertices':
      arr = parseText(child.text, 'f')
      num = 0
      for i in vertices:
        i.co.x = arr[3*num]
        i.co.y = arr[3*num+1]
        i.co.z = arr[3*num+2]
        num = num+1
        i.select = True
    elif child.tag == 'normals':
      arr = parseText(child.text, 'f')
      num = 0
      for i in vertices:
        i.normal.x = arr[3*num]
        i.normal.y = arr[3*num+1]
        i.normal.z = arr[3*num+2]
        num = num+1
    elif child.tag == 'primitive-collection':
      arr = parseText(child.text, 'I')
      size = len(arr)
      polys.add(size/3)
      for i in polys:
        i.loop_total = 3
        for j in range(3):
          i.vertices[j] = arr[3*num + j]
    elif child.tag == 'texcoords':
      arr = parseText(child.text, 'f')
      size = len(arr)
      mapping = A.array('I')
      mapping.append(len(vertices))

#UI linker for Blender
class TIPSImportOperator(bpy.types.Operator):
  """An operator that imports all the XML data in TIPS format"""
  bl_idname = "import_scene.tipsxml"
  bl_label = "Import from TIPS XML format"

  filepath = bpy.props.StringProperty(subtype="FILE_PATH")

  @classmethod
  def poll(cls, context):
    return context.object is not None

  def execute(self, context):
    file = open(self.filepath)
    importFromXML(file)
    file.close()
    return {'FINISHED'}

  def invoke(self, context, event):
    context.window_manager.fileselect_add(self)
    return {'RUNNING_MODAL'}

# Only needed if you want to add into a dynamic menu
def menu_func(self, context):
    self.layout.operator_context = 'INVOKE_DEFAULT'
    self.layout.operator(TIPSImportOperator.bl_idname, text="TIPS XML format")

    
def register():
    bpy.utils.register_class(TIPSImportOperator)
    #bpy.types.INFO_MT_file_import.append(menu_func)
   
def unregister():
    bpy.utils.unregister_class(TIPSImportOperator)
    #bpy.types.INFO_MT_file_import.remove(menu_func)

if __name__ == "__main__":
    register()

O.import_scene.tipsxml('INVOKE_DEFAULT')