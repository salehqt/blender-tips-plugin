import bpy

#class CollectionData(bpy.types.AnyType):
#    items = [ 5, 4, 3]
#    active = 0

class TIPSPanel(bpy.types.Panel):
    bl_category = "TIPS"
    bl_idname = "TIPS_UI_Panel"
    bl_label = "TIPS UI Panel"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'TOOLS'
    
    @classmethod
    def poll(cls, context):
      return (context is not None)

    def draw(self, context):
      layout = self.layout
      
      col = layout.column(align=True)
      col.label(text="Export: ")
      col.operator("export.tips_xml_model", text="Selected Model")
      col.operator("export.tips_xml_scene", text="Selected Scene")
      col.operator("export.tips_xml_all", text="All Scenes")
      col.operator("export.tips_xml_library", text = "Library")
      
      col = layout.column(align=True)
      col.label(text="Import: ")
      col.operator("import.tips_xml", text="TIPS XML file")

      #col = layout.column(align=True)
      #data = CollectionData()
      #col.template_list('UI_UL_list', "", data, 'items', data,  'active', rows=5, maxrows=5, type='DEFAULT')
      
      col = layout.column(align=True)
      col.label(text="List of Objects: ")
      box = layout.box();
      for i in bpy.data.objects:
        col = box.column(align=True)
        #col.label(text=i.name)
        edit = col.operator("object.add_named", text = i.name, emboss=False)
        edit.linked = True
        #edit.name = ""
'''      
def register():
  bpy.utils.register_class(TIPSPanel)
  
def unregister():
  bpy.utils.unregister_class(TIPSPanel)

if __name__ == "__main__":
  register()'''
