import bpy


def main(context):
    for ob in context.scene.objects:
        print(ob)


class SimpleOperator(bpy.types.Operator):
    """Tooltip"""
    bl_idname = "object.simple_operator"
    bl_label = "Simple Object Operator TEST"

    @classmethod
    def poll(cls, context):
        return context.active_object is not None

    def execute(self, context):
        main(context)
        return {'FINISHED'}


def register():
    bpy.utils.register_class(SimpleOperator)


def unregister():
    bpy.utils.unregister_class(SimpleOperator)

def menu_draw(self, context):
    self.layout.operator("object.simple_operator")



if __name__ == "__main__":
    register()

    # test call
    #bpy.ops.object.simple_operator()
    bpy.types.INFO_MT_file_export.append(menu_draw)