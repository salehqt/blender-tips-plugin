import bpy
import xml.etree.ElementTree as ET
import array
import os

def trunc(num):
  return "%.3f" % num
  
#generic iterator
def iterate(xmlNode, comp, type = 'f'):
  ret = ""
  for i in comp:
    if type == 'f':
      ret += str(trunc(i)) + " "
    else:
      print("Not supported")
  xmlNode.text = ret

def writeToFile(filepath, root):
  file = open(filepath, 'wb')
  ET.ElementTree(root).write(file)
  file.close()  
  
def exportToXML(mode):
  ## The XML namespace causes all the tag names to become FQN when
  ## importing, until we find out how to elimitae the namespaces when
  ## importing, we will not use xmlns.
  #root.set("xmlns", "http://surflab.cise.ufl.edu/TIPS/scene")
  
  
  if mode == 'scene':
    return serializeScene(bpy.context.scene)
  elif mode == 'object':
    return serializeObject(bpy.context.object)
  elif mode=='all':
    root = ET.Element("all-scenes");
    for s in bpy.data.scenes:
      root.append(serializeScene(s))
    return root
  else:
    root = ET.Element("empty")
    return root

def createLibrary(dir, root):
  scene = root.find('scene')
  lib = ET.Element("list")
  text = "\n"
  num = 0
  for child in scene:
    if child.tag == 'object':
      filename = child.attrib['name']
      filename = filename.replace(":", "_")
      text += filename + "\n"
      num = num + 1
      newchild = ET.Element("Description")
      newchild.set("content_type", 'object')
      newchild.append(child)
      writeToFile(dir+filename+".xml", newchild)
  lib.text = text;
  lib.set("total", str(num))
  writeToFile(dir+"List.xml", lib)
  
def serializeScene(obj):
  tag = ET.Element("scene")
  for o in obj.objects:
    if o.parent == None:
        tag.append(serializeObject(o))
  return tag
 
#deal with generic object
def serializeObject(obj):
  if obj.type == 'MESH':
    tag = ET.Element("mesh")
    for i in obj.data.materials:
      serializeMaterial(i, tag)
    serializeGeometry(obj.data, tag)
  elif obj.type == 'LAMP':
    tag = ET.Element("lamp")
  elif obj.type == 'CAMERA':
    tag = ET.Element("camera")
  else:
    tag = ET.Element("group")

  tag.set("name", obj.name)
  tag.set("type", obj.type)
  obj.rotation_mode = "QUATERNION"
 
  location = ET.SubElement(tag, "location")
  iterate(location, obj.location)
 
  rotation = ET.SubElement(tag, "rotation")
  iterate(rotation, obj.rotation_quaternion)
 
  scale = ET.SubElement(tag, "scale")
  iterate(scale, obj.scale)
  

  for c in obj.children:
    tag.append(serializeObject(c))
  
  serializeCustomProperties(obj, tag)
  
  return tag

#deal with material
def serializeMaterial(obj, parent):
  tag = ET.SubElement(parent, "material")
  diffuse = ET.SubElement(tag, "color")
  diffuse.set("type", "diffuse")
  iterate(diffuse, obj.diffuse_color)
  
  specular = ET.SubElement(tag, "color")
  specular.set("type", "specular")
  iterate(specular, obj.specular_color)

  ambient = ET.SubElement(tag, "ambient")
  ambient.text = str(obj.ambient);

  for i in obj.texture_slots:
    if(i != None): 
      e = ET.SubElement(tag, "texture")
      tex = i.texture
      if(tex.type == 'IMAGE'):
        e.set("filename", bpy.path.abspath(tex.image.filepath))
  return tag
  
#deal with geometry
def serializeGeometry(obj, parent):
  tag = ET.SubElement(parent, "geometry")
  vs = ET.SubElement(tag, "vertices")
  writ = ""
  if len(obj.uv_layers) >= 1 :
    uvl = obj.uv_layers[0].data
    vs.set("format", "v3,n3,t2")

    # allocate a mapping between vertex indices and loop indices
    mapping = array.array('I')
    for i in obj.vertices:
      mapping.append(0)
    for  l in obj.loops:
      mapping[l.vertex_index] = l.index

    for i, v in enumerate(obj.vertices):
      writ += str(v.co[0]) + " " + str(v.co[1]) + " " + str(v.co[2]) + " ";
      writ += str(v.normal.x) + " " + str(v.normal.y) + " " + str(v.normal.z) + " ";
      writ += str(uvl[mapping[i]].uv.x) + " " + str(uvl[mapping[i]].uv.y) + "  ";
  else:    
    vs.set("format", "v3,n3")
    for i, v in enumerate(obj.vertices):
      writ += str(v.co[0]) + " " + str(v.co[1]) + " " + str(v.co[2]) + " ";
      writ += str(v.normal.x) + " " + str(v.normal.y) + " " + str(v.normal.z) + " ";

  vs.text = writ
  
  obj.update(calc_tessface=True)
  def serialize_ngons(n):
    w = ""
    for i in obj.tessfaces:
      if len(i.vertices) == n:
        for x in i.vertices :
          w += str(x) + " "
    return w

  # Export triangles first
  triangle_text = serialize_ngons(3)
  if triangle_text != "":
    e = ET.SubElement(tag,"primitive-collection", { "type":"triangles"})
    e.text = triangle_text

  # Export quads now
  quad_text = serialize_ngons(4)
  if quad_text != "":
    e = ET.SubElement(tag,"primitive-collection", { "type":"quads"})
    e.text = quad_text

  return tag
  
def serializeCustomProperties(obj, parent):
  for key, value in obj.items():
    if key == '_RNA_UI':
      continue
    customNode = ET.SubElement(parent, "custom_property")
    customNode.set("name", str(key))
    customNode.set("type", obj.type)
    customNode.set("value", str(value))

class TIPSExportModelOperator(bpy.types.Operator):
  """An operator that exports selected model as XML data"""
  bl_idname = "export.tips_xml_model"
  bl_label = "Export selected model to TIPS XML format"

  filepath = bpy.props.StringProperty(subtype="FILE_PATH")
    
  @classmethod
  def poll(cls, context):
    return context is not None

  def execute(self, context):
    root = exportToXML('object')
    writeToFile(self.filepath, root)
    return {'FINISHED'}

  def invoke(self, context, event):
    context.window_manager.fileselect_add(self)
    return {'RUNNING_MODAL'}

class TIPSExportSceneOperator(bpy.types.Operator):
  """An operator that exports selected scene as XML data"""
  bl_idname = "export.tips_xml_scene"
  bl_label = "Export selected scene to TIPS XML format"

  filepath = bpy.props.StringProperty(subtype="FILE_PATH")

  @classmethod
  def poll(cls, context):
    return context is not None

  def execute(self, context):
    root = exportToXML('scene')
    writeToFile(self.filepath, root)
    return {'FINISHED'}

  def invoke(self, context, event):
    context.window_manager.fileselect_add(self)
    return {'RUNNING_MODAL'}    
    
class TIPSExportAllScenesOperator(bpy.types.Operator):
  """An operator that exports all scenes as XML data"""
  bl_idname = "export.tips_xml_all"
  bl_label = "Export all scenes to TIPS XML format"

  filepath = bpy.props.StringProperty(subtype="FILE_PATH")

  @classmethod
  def poll(cls, context):
    return context is not None

  def execute(self, context):
    root = exportToXML('all')
    writeToFile(self.filepath, root)
    return {'FINISHED'}

  def invoke(self, context, event):
    context.window_manager.fileselect_add(self)
    return {'RUNNING_MODAL'}

class TIPSExportLibraryOperator(bpy.types.Operator):
  bl_idname = "export.tips_xml_library"
  bl_label = "Export models in selected scene as a library"
  
  filepath = bpy.props.StringProperty(subtype="FILE_PATH")
  filename = bpy.props.StringProperty(subtype="FILE_NAME")
  
  @classmethod
  def poll(cls, context):
    return context is not None

  def execute(self, context):
    dirList = self.filepath.split("\\")
    dir = ""
    for i in dirList:
      if i != self.filename:
        dir += i + "\\";
    print(dir)
    root = exportToXML('scene')
    createLibrary(dir, root)
    return {'FINISHED'}

  def invoke(self, context, event):
    context.window_manager.fileselect_add(self)
    return {'RUNNING_MODAL'}
